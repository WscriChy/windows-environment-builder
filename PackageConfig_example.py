#!/usr/bin/env python
# encoding: utf-8

from Builder.Core.Object import Object

#from Builder.Packages.BinUtilsBuilder import BinUtilsBuilder
from Builder.Packages.ZLibBuilder import ZLibBuilder
from Builder.Packages.BoostBuilder import BoostBuilder
from Builder.Packages.Qt4Builder import Qt4Builder
from Builder.Packages.Qt5Builder import Qt5Builder
from Builder.Packages.SQLiteBuilder import SQLiteBuilder
from Builder.Packages.ODBBuilder import ODBBuilder
from Builder.Packages.ODBSQLiteBuilder import ODBSQLiteBuilder
from Builder.Packages.ODBQtBuilder import ODBQtBuilder

import copy

packageConfig = Object(SOURCE            = "C:/Users/CanisMajor/Downloads/Soft",
                    BUILD                = "C:/Users/CanisMajor/Downloads/Trash/build",
                    INSTALL              = "D:/Software",
                    J                    = 8,

                    HOST_ARCH            = "x86_64",
                    HOST_VENDOR          = "w64",
                    HOST_OS              = "mingw32",
                    HOST                 = "x86_64-w64-mingw32",
                    TARGET_ARCH          = "x86_64",
                    TARGET_VENDOR        = "w64",
                    TARGET_OS            = "mingw32",
                    TARGET               = "x86_64-ws-mingw32",
                    CRT_CONFIG           = "--disable-lib32 --enable-lib64",
                    SHORT_NAME           = "MinGW64",
                    LONG_NAME            = "MinGW64MinGW64",

                    OPTIMIZATION_FLAGS   = "-O3 -fomit-frame-pointer -funroll-loops",
                    STATIC_RUNTIME_FLAGS = "-static-libstdc++ -static-libgcc",

                    BJAM_PREFIX         = "Bjam",
                    BJAM_VERSION        = "1.54.0",

                    BOOST_PREFIX         = "Boost",
                    BOOST_VERSION        = "1.54.0",

                    QT4_PREFIX            = "Qt4",
                    QT4_VERSION           = "4.8.5",

                    QT5_PREFIX            = "Qt5",
                    QT5_VERSION           = "5.1.1",

                    ZLIB_PREFIX          = "ZLib",
                    ZLIB_VERSION         = "1.2.8",

                    SQLITE_PREFIX        = "SQLite",
                    SQLITE_VERSION       = "3.8.0",

                    BINUTILS_PREFIX      = "BinUtils",
                    BINUTILS_VERSION     = "2.24.51",

                    ODB_PREFIX           = "ODB",
                    ODB_VERSION          = "2.2.3",

                    ODBSQLITE_PREFIX     = "ODBSQLite",
                    ODBSQLITE_VERSION    = "2.2.3",

                    ODBQT_PREFIX         = "ODBQt",
                    ODBQT_VERSION        = "2.2.1",
                    )

# binUtilsBuilder = BinUtilsBuilder(config = copy.deepcopy(packageConfig))
# binUtilsBuilder.fullCycle()

zLibBuilder = ZLibBuilder(config = copy.deepcopy(packageConfig))

boostBuilder = BoostBuilder(config = copy.deepcopy(packageConfig))

qt4Builder = Qt4Builder(config = copy.deepcopy(packageConfig))

qt5Builder = Qt5Builder(config = copy.deepcopy(packageConfig))

sqliteBuilder = SQLiteBuilder(config = copy.deepcopy(packageConfig))

odbBuilder = ODBBuilder(config = copy.deepcopy(packageConfig))

odbSQLiteBuilder = ODBSQLiteBuilder(config = copy.deepcopy(packageConfig))

odbQtBuilder = ODBQtBuilder(config = copy.deepcopy(packageConfig))

def main():
  boostBuilder.fullCycle()
  qt4Builder.fullCycle()
  qt5Builder.fullCycle()
  odbBuilder.fullCycle()
  odbQtBuilder.fullCycle()
  odbSQLiteBuilder.fullCycle()

if __name__ == "__main__":
    main()
