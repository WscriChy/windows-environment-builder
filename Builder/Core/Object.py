#!/usr/bin/env python
# encoding: utf-8

# Then you can call it like this:
#
# e = Employee({"name": "abc", "age": 32})
# or like this:
#
# e = Employee(name="abc", age=32)
# or even like this:
#
# employee_template = {"role": "minion"}
# e = Employee(employee_template, name="abc", age=32)

class Object(object):

  def __init__(self, *args, **argd):
    for dictionary in args:
      for key in dictionary:
        setattr(self, key, dictionary[key])

    for key in argd:
        setattr(self, key, argd[key])

    pass

  def __getitem__(self, name):
    return getattr(self, name)

  def __setitem__(self, name, value):
    setattr(self, name, value)

 # def __init__(self, obj):
 #   for key, value in obj.__dict__.items():
 #     setattr(self, key, value)

  pass
# def __init__(self,*args,**argd):
 #   self.__dict__(self,*args,**argd);

 # def __getattr__(self,name):
 #   return self[name]

 # def __setattr__(self,name,value):
 #   raise AttributeError,"Attribute '%s' of '%s' object cannot be set"%(name,self.__class__.__name__)

 # def __delattr__(self,name):
 #   raise AttributeError,"Attribute '%s' of '%s' object cannot be deleted"%(name,self.__class__.__name__)
