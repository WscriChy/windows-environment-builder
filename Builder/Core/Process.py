#!/usr/bin/env python
# encoding: utf-8

from Object import Object
from Output import Output

import sys
import subprocess

class Process(Object):

  def __init__(self, *args, **argd):
    super(Process, self).__init__(*args, **argd)

  def run(self, command):
    if hasattr(self, 'env'):
      process = subprocess.Popen(command,
                                 cwd = self.cwd,
                                 env = self.env,
                                 stdout=Output(sys.__stdout__),
                                 stderr=Output(sys.__stderr__),
                                 stdin =sys.stdin,
                                 shell = True)
    else:
      process = subprocess.Popen(command,
                                 cwd = self.cwd,
                                 stdout=Output(sys.__stdout__),
                                 stderr=Output(sys.__stderr__),
                                 stdin =sys.stdin,
                                 shell = True)
    #while(True):
    #reads,writes,excs = select.select(
    #    [process.stdout, process.stderr],
    #    [process.stdin], [], 1)
    #for r in reads:
    #  stdout = r.read(1)
    #  self.StdOutReader(stdout)
    #for w in writes:
    #  w.write('a')
    #stdout, stderr = process.communicate(input='Ouuupppss!')
    #self.StdOutReader(stdout)
    #self.StdErrReader(stderr)
    retcode = process.wait() #returns None while subprocess is running
    if(retcode is not None):
      return retcode
