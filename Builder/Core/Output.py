#!/usr/bin/env python
# encoding: utf-8

from Object import Object

import sys

LOG_FILE = "build.log"

def log(*objects):
  sys.stdout = Output(sys.__stdout__)
  for i in objects:
    print(i)
  sys.stdout = sys.__stdout__
  pass

class Output(object):
  def __init__(self, base):
    self.base = base
    self.log = open(LOG_FILE,'a')

  def write(self, x):
    self.base.write(x)
    self.log.write(x)

  def close(self):
    self.log.close()
    return self.base.close()

  def flush(self):
    self.log.flush()
    return self.base.flush()

  def fileno(self):
    self.log.fileno()
    return self.base.fileno()
