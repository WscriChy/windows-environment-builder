#!/usr/bin/env python
# encoding: utf-8

from Builder import Builder
from Builder import PathType

from Process import Process
from Object import Object
from Output import log

class Configurator(Object):

  def __init__(self, *args, **argd):
    self.env = lambda self: None
    self.cwd = lambda self: None
    self.commandGenerator = lambda self: None
    super(Configurator, self).__init__(*args, **argd)
    pass

class BasicBuilder(Builder):

  def __init__(self, *args, **argd):
    super(BasicBuilder, self).__init__(*args, **argd)
    self.configurator = Configurator()
    self.dependencies = Object()
    if not hasattr(self, "PREFIX"):
      prefix = self.builderName.upper() + "_PREFIX"
      if not hasattr(self.config, prefix):
        prefix = self.builderName.upper()

      self.PREFIX = self.config[prefix]

    self.isPrefixAbs = os.path.isabs(self.PREFIX)

    version = self.builderName.upper() + "_VERSION"
    if not hasattr(self.config, version):
      self.VERSION = ""
    else:
      self.VERSION = self.config[version]

  def init(self):
    if not hasattr(self, "INSTALL_PATH"):
      if self.isPrefixAbs:
        self.INSTALL_PATH = self.buildPath(self.PREFIX,
                                           self.VERSION)
      else:
        self.INSTALL_PATH = self.buildPath(self.config.INSTALL,
                                                 self.PREFIX,
                                                 self.VERSION)

    if not hasattr(self, "BIN_PATH"):
      self.BIN_PATH = self.buildPath(self.INSTALL_PATH, "bin", pathType = PathType.WindowsPath)

    if not hasattr(self, "INCLUDE_PATH"):
      self.INCLUDE_PATH = self.buildPath(self.INSTALL_PATH, "include", PathType.WindowsPath)

    if not hasattr(self, "LIB_PATH"):
      self.LIB_PATH = self.buildPath(self.INSTALL_PATH, "lib", PathType.WindowsPath)

    self.checkInstallation(self.INSTALL_PATH)
    pass

  def _updateDependencies(self):
    publicAttrs = (dependency for dependency in dir(self.dependencies)
        if not dependency.startswith('_') and not dependency.startswith('__'))
    for dependency in publicAttrs:
      self.dependencies[dependency].fullCycle()

  def _configure(self):
    if self.installed:
      return

    if not hasattr(self, "SOURCE_PATH"):
      if not hasattr(self.config, "SOURCE"):
        self.SOURCE_PATH  = self.checkPath(self.PREFIX, self.VERSION)
      else:
        self.SOURCE_PATH  = self.checkPath(
            self.config.SOURCE,
            self.PREFIX,
            self.VERSION)

    if self.isPrefixAbs:
      self.BUILD_PATH   = self.buildPath(self.config.BUILD,
                                         self.builderName,
                                         self.VERSION)
    else:
      self.BUILD_PATH   = self.buildPath(self.config.BUILD,
                                         self.PREFIX,
                                         self.VERSION)

    self.cleanFiles(self.BUILD_PATH)

    command = self.configurator.commandGenerator(self)
    if command is None: return
    if self.configurator.cwd(self) is None:
      self.configurator.cwd = lambda self: self.BUILD_PATH
    env = self.configurator.env(self)
    if env is not None:
      log("Configuring %s with modified environment:\n%s" % (self.builderName, env))

    log("Configuring %s with the command:\n%s" % (self.builderName, command))

    if self.RETURN_CODE != 0:
      return
    process = Process(cwd = self.configurator.cwd(self),
                      env = self.configurator.env(self))
    self.configurator = Configurator()
    returnCode = process.run(command)
    self.RETURN_CODE = returnCode
    pass

  def _build(self):
    if self.installed:
      return
    if self.RETURN_CODE != 0:
      return
    command = self.configurator.commandGenerator(self)
    if command is None: return
    if self.configurator.cwd(self) is None:
      self.configurator.cwd = lambda self: self.BUILD_PATH
    process = Process(cwd = self.configurator.cwd(self),
                      env = self.configurator.env(self))
    self.configurator = Configurator()
    returnCode = process.run(command)
    self.RETURN_CODE = returnCode
    pass

  def _install(self):
    if self.installed:
      return
    if self.RETURN_CODE != 0:
      return
    command = self.configurator.commandGenerator(self)
    if command is None: return
    if self.configurator.cwd(self) is None:
      self.configurator.cwd = lambda self: self.BUILD_PATH
    process = Process(cwd = self.configurator.cwd(self),
                      env = self.configurator.env(self))
    self.configurator = Configurator()
    returnCode = process.run(command)
    log("Install '%s' with the command:\n%s" % (self.builderName, command))
    self.RETURN_CODE = returnCode
    pass

  def fullCycle(self):
    self.configure()
    self.build()
    self.install()
    pass
