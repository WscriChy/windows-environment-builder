#!/usr/bin/env python
# encoding: utf-8

import os, sys, re, win32api, win32con

def getEnvironmentVariable(varname, isCurrentUser = True, default=None):
  v = default
  try:
    rkey = None
    if isCurrentUser:
      rkey = win32api.RegOpenKeyEx(win32con.HKEY_CURRENT_USER, 'Environment', 0, win32con.KEY_WRITE)
    else:
      rkey = win32api.RegOpenKey(win32con.HKEY_LOCAL_MACHINE, 'SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment')
    try:
      v = str(win32api.RegQueryValueEx(rkey, varname)[0])
      v = win32api.ExpandEnvironmentStrings(v)
    except:
      pass
  finally:
    win32api.RegCloseKey(rkey)
  return v

def setEnvironmentVariable(varname, value, isCurrentUser = True):
  rkey = None
  try:
    rkey = None
    if isCurrentUser:
      rkey = win32api.RegOpenKeyEx(win32con.HKEY_CURRENT_USER, 'Environment', 0, win32con.KEY_WRITE)
    else:
      rkey = win32api.RegOpenKeyEx(win32con.HKEY_LOCAL_MACHINE, 'SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment', 0, win32con.KEY_WRITE)
    match = re.search(".*%([A-Za-z_\-0-9]+)%.*", value)
    try:
      if match:
        win32api.RegSetValueEx(rkey, varname, 0, win32con.REG_EXPAND_SZ, value)
      else:
        win32api.RegSetValueEx(rkey, varname, 0, win32con.REG_SZ, value)
      return True
    except Exception, (error):
      pass
  finally:
    win32api.RegCloseKey(rkey)
  return False

def setConfiguration(config):
  publicAttrs = (entity for entity in dir(config)
    if not entity.startswith('_') and not entity.startswith('__'))
  for entity in publicAttrs:
    value = ""
    if type(config[entity]) is list:
      value  = ";".join(config[entity])
    else:
      value = config[entity]
    setEnvironmentVariable(entity, value)
