#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

from SQLiteBuilder import SQLiteBuilder
from ODBBuilder import ODBBuilder

class ODBSQLiteBuilder(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "ODBSQLite"
    super(ODBSQLiteBuilder, self).__init__(*args, **argd)
    self.dependencies.SQLiteBuilder = SQLiteBuilder(config = self.config)
    self.dependencies.ODBBuilder = ODBBuilder(config = self.config)
    self._updateDependencies()

  def configure(self):
    self.configurator.commandGenerator =  lambda self: (
        'sh.exe -c "%s/configure '
        '--build=%s '
        'CXXFLAGS=\'-O3 -I%s -I%s\' '
        'LDFLAGS=\'%s -L%s -L%s\' '
        '--prefix=\'%s\'"' %
        (self.SOURCE_PATH,
         self.config.HOST,
         self.preparePath(self.dependencies.ODBBuilder.INCLUDE_PATH),
         self.preparePath(self.dependencies.SQLiteBuilder.INCLUDE_PATH),
         self.config.STATIC_RUNTIME_FLAGS,
         self.preparePath(self.dependencies.ODBBuilder.LIB_PATH),
         self.preparePath(self.dependencies.SQLiteBuilder.LIB_PATH),
         self.INSTALL_PATH))

    self._configure()
    pass

  def build(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i' % (self.config.J))
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i install' % (self.config.J))
    self._install()
    pass

