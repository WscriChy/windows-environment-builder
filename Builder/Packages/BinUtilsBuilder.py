#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

class BinUtilsBuilder(BasicBuilder):

  def __init__(self,*args, **argd):
    self.builderName = "BinUtils"
    super(BinUtilsBuilder, self).__init__(*args, **argd)

  def configure(self):
    self.configurator.commandGenerator =  lambda self: (
                'sh -c "%s/configure '
                '--prefix=%s '
                '--build=%s '
                '--target=%s '
                #'--enable-64-bit-bfd '
                #'--disable-multilib '
                #'--disable-nls '
                # Bad reaction at -O3
                # 'CC=\'-O3\' '
                # 'CFLAGS=\'-O3\' '
                'LDFLAGS=\'%s\'"' %
                (self.SOURCE_PATH,
                 self.INSTALL_PATH,
                 self.config.HOST,
                 self.config.TARGET,
                 self.config.STATIC_RUNTIME_FLAGS))
    self._configure()
    pass

  def build(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i' % (self.config.J))
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i install' % (self.config.J))
    self._install()
    pass
