#!/usr/bin/env python
# encoding: utf-8

from ..Core.Builder import Builder

from ..Core.Process import Process

class ZLibBuilder(Builder):

  def __init__(self,*args,**argd):
    super(ZLibBuilder, self).__init__(*args,**argd)
    self._inittialization()

  def _inittialization(self):
    self.ZLIB_INSTALL_PATH = self.buildPath(
                   self.config.INSTALL,
                   self.config.ZLIB_PREFIX,
                   self.config.ZLIB_VERSION)

    self.ZLIB_INCLUDE_PATH = self.buildPath(
                   self.ZLIB_INSTALL_PATH,
                   "include")

    self.ZLIB_LIB_PATH = self.buildPath(
                   self.ZLIB_INSTALL_PATH,
                   "lib")
    pass

  def configure(self):
    self.ZLIB_SOURCE_PATH  = self.checkPath(
                   self.config.SOURCE,
                   self.config.ZLIB_PREFIX,
                   self.config.ZLIB_VERSION)

    self.ZLIB_BUILD_PATH   = self.buildPath(
                   self.config.BUILD,
                   self.config.ZLIB_PREFIX,
                   self.config.ZLIB_VERSION)

    command =  ('cmake '
                '-DCMAKE_BUILD_TYPE=Release '
                '-DCMAKE_INSTALL_PREFIX="%s" '
                '-DCMAKE_C_FLAGS="-O3" '
                '-DCMAKE_CXX_FLAGS="-O3" '
                '-DCMAKE_EXE_LINKER_FLAGS=" '
                '-static-libstdc++ -static-libgcc -static" '
                '-DCMAKE_SHARED_LINKER_FLAGS=" '
                '-static-libstdc++ -static-libgcc -static" '
                '-G "Ninja" %s' %
                (self.ZLIB_INSTALL_PATH,
                self.ZLIB_SOURCE_PATH))
    print("Configuring ZLib with the command:\n%s" % command);

    if self.RETURN_CODE != 0:
      return
    process = Process(cwd = self.ZLIB_BUILD_PATH)
    returnCode = process.run(command)
    self.RETURN_CODE = returnCode
    pass

  def build(self):
    if self.RETURN_CODE != 0:
      return
    process = Process(cwd = self.ZLIB_BUILD_PATH)
    returnCode = process.run('ninja')
    self.RETURN_CODE = returnCode
    pass

  def install(self):
    if self.RETURN_CODE != 0:
      return
    process = Process(cwd = self.ZLIB_BUILD_PATH)
    returnCode = process.run('ninja install')
    self.RETURN_CODE = returnCode
    pass


