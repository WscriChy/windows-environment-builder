#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

from ..Core.Builder import PathType
from ..Core.Process import Process

class BjamBuilder(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "Bjam"
    self.pathType = PathType.WindowsPath
    super(BjamBuilder, self).__init__(*args, **argd)
    self.B2 = self.preparePath(self.BIN_PATH, "b2.exe")
    self.BJAM = self.preparePath(self.BIN_PATH, "bjam.exe")

  def configure(self):
    if self.installed:
      return

    self.SOURCE_PATH = self.checkPath(
        self.config.SOURCE,
        self.config.BOOST_PREFIX,
        self.config.BOOST_VERSION,
        "tools\\build\\v2")
    commandPath1 = self.checkPath(
        self.SOURCE_PATH,
        "bootstrap.bat")
    commandPath2 = self.preparePath(
        self.SOURCE_PATH,
        "b2")

    command1 =  ('cmd.exe /C "%s"' % (commandPath1))

    command2 =  ('b2.exe '
                 'address-model=64 '
                 'toolset=gcc '
                 '--prefix=%s '
                 'install' % (self.INSTALL_PATH))

    if self.RETURN_CODE != 0:
      return

    print("Configuring %s with the command1:\n%s" % (self.builderName, command1))

    process = Process(cwd = self.SOURCE_PATH)
    returnCode = process.run(command1)
    self.RETURN_CODE = returnCode

    if self.RETURN_CODE != 0:
      return

    print("Configuring %s with the command2:\n%s" % (self.builderName, command2))

    returnCode = process.run(command2)
    self.RETURN_CODE = returnCode
    pass

  def build(self):
    pass

  def install(self):
    pass
