#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder
from ..Core.BasicBuilder import PathType

from Uni import Uni

def _commandGenerator(self):
  command = ("cmake -G \"Ninja\" "
      "-DCMAKE_BUILD_TYPE=Release "
      "-DCMAKE_INSTALL_PREFIX=\"%s\" "
      "-DCMAKE_CXX_FLAGS=\"%s\" "
      "-DCMAKE_C_FLAGS=\"%s\" "
      "-DCMAKE_SHARED_LINKER_FLAGS=\"%s -static\" "
      "-DCMAKE_EXE_LINKER_FLAGS=\"%s -static\" "
      "%s"
      ) % (self.INSTALL_PATH,
          self.config.CXX_OPTIMIZATION_FLAGS,
          self.config.CXX_OPTIMIZATION_FLAGS,
          self.config.STATIC_RUNTIME_FLAGS,
          self.config.STATIC_RUNTIME_FLAGS,
          self.SOURCE_PATH)
  return command

class DependencyManagerBuilder(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "DependencyManager"
    super(DependencyManagerBuilder, self).__init__(*args, **argd)
    self.dependencies.Uni = Uni(config = self.config)
    self._updateDependencies()

  def configure(self):
    self.configurator.commandGenerator =  _commandGenerator
    self._configure()
    pass

  def build(self):
    self.configurator.commandGenerator = lambda self: ('ninja -j %i' % (self.config.J))
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('ninja -j %i install' % (self.config.J))
    self._install()
    pass
