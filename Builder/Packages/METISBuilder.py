#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

# This problem should be corrected if you modify the top-level CMakeLists.txt file:
#
# replace line 4:
# set(GKLIB_PATH "GKlib" CACHE PATH "path to GKlib")
#
# by
# set(GKLIB_PATH "${CMAKE_SOURCE_DIR}/GKlib" CACHE PATH "path to GKlib")
# cmake -G "MinGW Makefiles" -DCMAKE_CONFIGURATION-TYPES="Release" -DGKLIB_PATH="..\..\GKlib" ..\..

# gk_getopt.h removes __

#

class METISBuilder(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "METIS"
    super(METISBuilder, self).__init__(*args, **argd)

  def configure(self):
    self.configurator.commandGenerator =  lambda self: (
        ("cmake -G \"Ninja\" "
        "-DCMAKE_CONFIGURATION-TYPES=\"Release\" "
        "-DCMAKE_INSTALL_PREFIX:PATH=%s "
        "-DCMAKE_CXX_FLAGS=\"%s\" "
        "-DCMAKE_C_FLAGS=\"%s\" "
        "-DCMAKE_SHARED_LINKER_FLAGS=\"%s -static\" "
        "-DCMAKE_EXE_LINKER_FLAGS=\"%s -static\" "
        "-DGKLIB_PATH=\"%s\" "
        "%s"
        ) % (self.INSTALL_PATH,
             self.config.CXX_OPTIMIZATION_FLAGS,
             self.config.CXX_OPTIMIZATION_FLAGS,
             self.config.STATIC_RUNTIME_FLAGS,
             self.config.STATIC_RUNTIME_FLAGS,
             self.checkPath(self.SOURCE_PATH, "GKlib"),
             self.SOURCE_PATH))
    self._configure()
    pass

  def build(self):
    self.configurator.commandGenerator = lambda self: ('ninja')
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('ninja install')
    self._install()
    pass
