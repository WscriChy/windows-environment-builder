#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

def _commandGenerator(self):
  commandPath = self.checkPath(self.SOURCE_PATH, "source", "runConfigureICU")
  return ('sh -c "%s '
         'MinGW '
         '--prefix=%s '
         '--build=%s '
         '--host=%s"' %
         #'CFLAGS=\'%s\' '
         #'CXXFLAGS=\'%s\' '
         #'LDFLAGS=\'%s\'"' %
         (commandPath,
          self.INSTALL_PATH,
          self.config.HOST,
          self.config.HOST))
          #self.config.CXX_OPTIMIZATION_FLAGS,
          #self.config.CXX_OPTIMIZATION_FLAGS,
          #self.config.STATIC_RUNTIME_FLAGS))

class ICUBuilder(BasicBuilder):

  def __init__(self,*args, **argd):
    self.builderName = "ICU"
    super(ICUBuilder, self).__init__(*args, **argd)

  def configure(self):
    self.configurator.commandGenerator =  _commandGenerator
    self._configure()
    pass

  def build(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i' % (self.config.J))
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i install' % (self.config.J))
    self._install()
    pass
