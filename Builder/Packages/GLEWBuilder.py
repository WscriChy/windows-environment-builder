#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

class GLEWBuilder(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "GLEW"
    super(GLEWBuilder, self).__init__(*args,**argd)

  def configure(self):
    self._configure()

  def build(self):
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i '
                'SYSTEM=\'%s\' '
                'GLEW_DEST=\'%s\' install.all' %
                (self.config.J,
                "mingw",
                 self.INSTALL_PATH))
    self.configurator.cwd = lambda self:(self.SOURCE_PATH)
    self._install()
    pass
