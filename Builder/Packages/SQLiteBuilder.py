#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

class SQLiteBuilder(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "SQLite"
    super(SQLiteBuilder, self).__init__(*args,**argd)

  def configure(self):
    self.configurator.commandGenerator =  lambda self: ('sh.exe -c "%s/configure '
                '--build=%s '
                'CXXFLAGS=\'-O3\' '
                'LDFLAGS=\'%s\' '
                '--prefix=\'%s\'"' %
                (self.SOURCE_PATH,
                 self.config.HOST,
                 self.config.STATIC_RUNTIME_FLAGS,
                 self.INSTALL_PATH))
    self._configure()

  def build(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i' % (self.config.J))
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('make -j %i install' % (self.config.J))
    self._install()
    pass
