#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

from os.path import join as join

def _commandGenerator(self):
  command = ("cmake -G \"Ninja\" "
      "-DCMAKE_BUILD_TYPE=Release "
      "-DCMAKE_INSTALL_PREFIX=\"%s\" "
      "-DCMAKE_CXX_FLAGS=\"-DM_PI=3.14159265358979323846 -D_USE_MATH_DEFINES %s\" "
      "-DCMAKE_C_FLAGS=\"-DM_PI=3.14159265358979323846 -D_USE_MATH_DEFINES %s\" "
      "-DCMAKE_SHARED_LINKER_FLAGS=\"%s -static\" "
      "-DCMAKE_EXE_LINKER_FLAGS=\"%s -static\" "
      "%s"
      ) % (self.INSTALL_PATH,
          self.config.CXX_OPTIMIZATION_FLAGS,
          self.config.CXX_OPTIMIZATION_FLAGS,
          self.config.STATIC_RUNTIME_FLAGS,
          self.config.STATIC_RUNTIME_FLAGS,
          join(self.SOURCE_PATH))
  return command

class VMMLib(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "VMMLib"
    super(VMMLib, self).__init__(*args, **argd)

  def configure(self):
    self.configurator.commandGenerator =  _commandGenerator
    self._configure()
    pass

  def build(self):
    self.configurator.commandGenerator = lambda self: ('ninja -j %i' % (self.config.J))
    self._build()
    pass

  def install(self):
    self.configurator.commandGenerator = lambda self: ('ninja -j %i install' % (self.config.J))
    self._install()
    pass
