#!/usr/bin/env python
# encoding: utf-8

from ..Core.BasicBuilder import BasicBuilder

from ..Core.BasicBuilder import PathType

from BjamBuilder import BjamBuilder

class BoostBuilder(BasicBuilder):

  def __init__(self, *args, **argd):
    self.builderName = "Boost"
    self.pathType = PathType.WindowsPath
    super(BoostBuilder, self).__init__(*args, **argd)
    self.dependencies.BjamBuilder = BjamBuilder(config = self.config)
    self._updateDependencies()

  def configure(self):
    self.configurator.commandGenerator = lambda self: (
        '%s '
        '-j %i '
        '--build-dir=%s '
        '--build-type=complete '
        'variant=debug,release '
        'link=shared,static '
        'threading=single,multi '
        'address-model=64 '
        'toolset=gcc '
        'cxxflags=-O3 '
        'cflags=-O3 '
        #'linkflags=\'%s\' '
        'runtime-link=shared,static '
        '--prefix=%s install' %
        (self.dependencies.BjamBuilder.B2,
          self.config.J,
          self.BUILD_PATH,
          #self.config.STATIC_RUNTIME_FLAGS,
          self.INSTALL_PATH))
    self.configurator.cwd = lambda self: self.SOURCE_PATH
    self._configure()

  def build(self):
    pass

  def install(self):
    pass
