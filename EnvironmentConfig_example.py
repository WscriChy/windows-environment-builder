#!/usr/bin/env python
# encoding: utf-8

from Builder.Core.Object import Object
from Builder.Windows.EnvironmentVariable import setConfiguration

from PackageConfig import *

environmentConfig = Object(
    Boost_INCLUDE_DIR         = boostBuilder.INCLUDE_PATH,
    Boost_LIBRARY_DIR         = boostBuilder.LIB_PATH,
    BOOST_ROOT                = boostBuilder.INSTALL_PATH,
    SQLite_INCLUDE_DIR        = sqliteBuilder.INCLUDE_PATH,
    SQLite_LIBRARY_DIR        = sqliteBuilder.LIB_PATH,
    ODB_INCLUDE_DIR           = odbBuilder.INCLUDE_PATH,
    ODB_LIBRARY_DIR           = odbBuilder.LIB_PATH,
    ODBSQLITE_INCLUDE_DIR     = odbSQLiteBuilder.INCLUDE_PATH,
    ODBSQLITE_LIBRARY_DIR     = odbSQLiteBuilder.LIB_PATH,
    ODBQt_INCLUDE_DIR         = odbQtBuilder.INCLUDE_PATH,
    ODBQt_LIBRARY_DIR         = odbQtBuilder.LIB_PATH,
    Console_CONFIGURATION_DIR = "%UserProfile%\.console",
    Console_DIR               = "D:\\Software\\Msys2\\lib\\ConsoleZ",
    Eigen3_INCLUDE_DIR        = "D:\\Eigen3\\include",
    GCF_INCLUDE_DIR           = "E:\\Documents\\C++Projects\\Gcf\\include\\SDK",
    GCF_LIBRARY_DIR           = "E:\\Documents\\C++Projects\\Gcf\\lib",
    GLEW_INCLUDE_DIR          = "D:\\glew\\include",
    GLEW_LIBRARY_DIR          = "D:\\glew\\lib",
    HOME                      = "C:\\Users\\CanisMajor",
    JAVA_HOME                 = "C:\\Program Files\\Java\\jdk1.7.0_06",
    MSYS2_BINARY_DIR          = "D:\\Software\\Msys2\\bin",
    OpenCL_INCLUDE_DIR        = "E:\\Documents\\C++Projects\\Others\\OpenAPI\\OpenCL\\include",
    OpenCL_LIBRARY_DIR        = "E:\\Documents\\C++Projects\\Others\\OpenAPI\\OpenCL\\lib",
    QT_API = "pyside",
    QtDataBinding_INCLUDE_DIR = "E:\\Documents\\C++Projects\\QtDataBinding\\install\\include",
    QtDataBinding_LIBRARY_DIR = "E:\\Documents\\C++Projects\\QtDataBinding\\install\\lib",
    PATH                      = [
      boostBuilder.LIB_PATH,
      "D:\\Software\\ODBCompiler\\2.2.2\\bin",
      odbBuilder.BIN_PATH,
      odbSQLiteBuilder.BIN_PATH,
      odbQtBuilder.BIN_PATH,
      sqliteBuilder.BIN_PATH,
      qt5Builder.BIN_PATH,
      "D:\\vim\\bin",
      "D:\\python\\2.7",
      "D:\\python\\2.7\\Scripts",
      "D:\\Software\\MSys2\\bin",
      "D:\\Software\\MinGW64-x64-4.7.3-release-win32-sjlj-rev1\\bin",
      "C:\\Program Files (x86)\\NVIDIA Corporation\\PhysX\\Common",
      "D:\\glew\\bin",
      "D:\\MsMpi\\bin",
      "D:\\cmake\\bin",
      "D:\\ninja",
      "D:\\LLVM\\3.3\\bin",
      "D:\\xpdf\\bin64",
      "D:\\waf",
      "D:\\python\\3.3",
      "D:\\uncrustify",
      "D:\\apache-maven-3.0.4\\bin",
      "C:\\Program Files\\Java\\jdk1.7.0_06\\bin"
      ]
    )

def main():
  setConfiguration(environmentConfig)

if __name__ == "__main__":
    main()
